var lineReader = require('line-reader');
var fs = require('fs');

/*
Support function to format string more comfy way
*/
String.prototype.format = String.prototype.f = function(){
    var args = arguments;
    return this.replace(/\{(\d+)\}/g, function(m,n){
        return args[n] ? args[n] : m;
    });
};

/*
Read mt2 rules
And clean grammar2.txt (file with outputs)
*/
(function start() {
    // fs.writeFileSync("grammar2.txt", "");

    var counter = 0;
    lineReader.eachLine('mt2.txt', function(line, last) {
        if (line != "\n") func(line);

        if (last) {
            generate();
            return false; // stop reading
        }
    });
})();

function func(line) {
    var res = parseData(line);
    G.push(res);
}

function pr(arr) {
    arr.forEach( function (elem) {
        pr1(elem);
    });
}

function pr1(val){
    if (val === undefined) {
        fs.appendFileSync('grammar.txt', "\n");
    } else {
        fs.appendFileSync('grammar.txt', "{0}\n".f(val));
    }
}

var terminals = "01"; //+
var tape_symbols = "abcdefgh"; // +
var G = []; // -
var init_state = "q1"; // +
var all_states = ["q1", "q2", "q3", "q4", "q5", "q6", "q7", "q8", "q9", "q10", "q11",
    "q12", "q13", "q14", "q15", "q16", "q17", "qE", "qS"]; // +
var finite_states = ["qE", "qS"]; //+
//non_finite_states = all_states - finite_states
var states = all_states.splice(0, 17); //+
var axiom = 'A1'; // start non-terminal
var axiom2 = 'A2';  // additional state


function containe_tr (state_before, value_before, state_after, value_after, shift) {
    var amount = 0;

    G.forEach( function(rule) {
        if (rule === undefined) return;
        // if (rule[0] == value_before) console.log("true");
        // if (rule[1] == state_before) console.log("true");
        // if (rule[2] == value_after) console.log("true");
        // if (rule[3] == state_after) console.log("true");
        // if (rule[4] == shift.toUpperCase()) console.log("true");

        console.log(rule[4] + " " + shift.toUpperCase());
        if (rule[0] == value_before &&
            rule[1] == state_before &&
            rule[2] == value_after &&
            rule[3] == state_after &&
            rule[4] == shift.toUpperCase())
        {
            amount++;
        }
    });

    return amount == 1;
}

function parseData(data) {
    if (data === "") return;
    var arrResult = [], arrTemp = [];
    arrTemp = data.split("->");
    arrResult.push(arrTemp[0][0]);
    arrResult.push(arrTemp[0].replace(/^(0|1|B|#|b|&|a)/, ""));
    arrResult.push(arrTemp[1][0]);
    arrResult[4] = arrTemp[1][arrTemp[1].length-1];
    arrTemp[1] = arrTemp[1].replace(/^(0|1|B|#|b|&|a)/, "");
    arrTemp[1] = arrTemp[1].replace(/(L|R)$/, "");
    arrResult[3] = arrTemp[1];

    return arrResult;
}

function generate () {
    var tempArr = [];
// 1:    A1 → [q0, ¢, a, a, $]
    for (var i = 0; i < terminals.length; i++) {
        tempArr.push("{0} -> [{1},&,{2},{2},#]".f(axiom, init_state, terminals[i]));
    }
    pr(tempArr);
    pr1();
    console.log("part 1/9 done")

// 2.1:  [q, ¢, X, a, $] → [¢, p, X, a, $],  if (p, ¢, R) ∈ δ(q, ¢) and q ∈ Q \ F
    var tempArr = [];
    for (var i = 0; i < states.length; i++) {
        for (var j = 0; j < states.length; j++) {
            for (var k = 0; k < tape_symbols.length; k++) {
                for (var l = 0; l < terminals.length; l++) {
                    if (containe_tr(states[i], '&', states[j], '&', 'r')) {
                        tempArr.push("[{0},&,{2},{3},#] -> [&,{1},{2},{3},#]"
                            .f(states[i], states[j], tape_symbols[k], terminals[l]));
                    }
                }
            }
        }
    }
    pr(tempArr);
    tempArr = [];
// 2.2:  [¢, q, X, a, $] → [p, ¢, Y, a, $],  if (p, Y, L) ∈ δ(q, X) and q ∈ Q \ F
    for (var i = 0; i < states.length; i++) {
        for (var j = 0; j < states.length; j++) {
            for (var k1 = 0; k1 < tape_symbols.length; k1++) {
                for (var k2 = 0; k2 < tape_symbols.length; k2++) {
                    for (var l = 0; l < terminals.length; l++) {
                        if (containe_tr(states[i], tape_symbols[k1], states[j], tape_symbols[k2], 'l')) {
                            tempArr.push("[&,{0},{2},{4},#] -> [{1},&,{3},{4},#]"
                                .f(states[i], states[j], tape_symbols[k1], tape_symbols[k2], terminals[l]));
                        }
                    }
                }
            }
        }
    }
    pr(tempArr);
    tempArr = [];
// 2.3:  [¢, q, X, a, $] → [¢, Y, a, p, $],  if (p, Y, R) ∈ δ(q, X) and q ∈ Q \ F
    for (var i = 0; i < states.length; i++) {
        for (var j = 0; j < states.length; j++) {
            for (var k1 = 0; k1 < tape_symbols.length; k1++) {
                for (var k2 = 0; k2 < tape_symbols.length; k2++) {
                    for (var l = 0; l < terminals.length; l++) {
                        if (containe_tr(states[i], tape_symbols[k1], states[j], tape_symbols[k2], 'r')) {
                            tempArr.push("[&,{0},{2},{4},#] -> [&,{3},{4},{1},#]"
                                .f(states[i], states[j], tape_symbols[k1], tape_symbols[k2], terminals[l]));
                        }
                    }
                }
            }
        }
    }
    pr(tempArr);
    tempArr = [];
// 2.4:  [¢, X, a, q, $] → [¢, p, X, a, $],  if (p, $, L) ∈ δ(q, $) and q ∈ Q \ F
    for (var i = 0; i < states.length; i++) {
        for (var j = 0; j < states.length; j++) {
            for (var k1 = 0; k1 < tape_symbols.length; k1++) {
                for (var k2 = 0; k2 < tape_symbols.length; k2++) {
                    for (var l = 0; l < terminals.length; l++) {
                        if (containe_tr(states[i], '#', states[j], '#', 'l')) {
                            tempArr.push("[&,{2},{4},{0},#] -> [&,{1},{2},{4},#]"
                                .f(states[i], states[j], tape_symbols[k1], tape_symbols[k2], terminals[l]));
                        }
                    }
                }
            }
        }
    }
    pr(tempArr);
    pr1();
    console.log("part 2/9 done");


// 3.1:  [q, ¢, X, a, $] → a, q ∈ F
    tempArr = [];
    for (var i = 0; i < finite_states.length; i++) {
        for (var j = 0; j < tape_symbols.length; j++) {
            for (var k = 0; k < terminals.length; k++) {
                tempArr.push("[{0},&,{1},{2},#] -> {2}"
                    .f(finite_states[i], tape_symbols[j], terminals[k]));
            }
        }
    }
    pr(tempArr);
    tempArr = [];
// 3.2:  [¢, q, X, a, $] → a, q ∈ F
    for (var i = 0; i < finite_states.length; i++) {
        for (var j = 0; j < tape_symbols.length; j++) {
            for (var k = 0; k < terminals.length; k++) {
                tempArr.push("[&,{0},{1},{2},#] -> {2}"
                    .f(finite_states[i], tape_symbols[j], terminals[k]));
            }
        }
    }
    pr(tempArr);
    tempArr = [];
// 3.3:  [¢, X, a, q, $] → a, q ∈ F
    for (var i = 0; i < finite_states.length; i++) {
        for (var j = 0; j < tape_symbols.length; j++) {
            for (var k = 0; k < terminals.length; k++) {
                tempArr.push("[&,{1},{2},{0},#] -> {2}"
                    .f(finite_states[i], tape_symbols[j], terminals[k]));
            }
        }
    }
    pr(tempArr);
    pr1();
    console.log("part 3/9 done");

// 4.1:  A1 → [q0, ¢, a, a] A2
    tempArr = [];
    for (var i = 0; i < terminals.length; i++) {
        tempArr.push("{0} -> [{1},&,{2},{2}]{3}".f(axiom, init_state, terminals[i], axiom2));
    }
    pr(tempArr);
    tempArr = [];
// 4.2:  A2 → [a, a] A2
    for (var i = 0; i < terminals.length; i++) {
        tempArr.push("{0} -> [{1},{1}]{0}".f(axiom2,terminals[i]));
    }
    pr(tempArr);
    tempArr = [];
// 4.3:  A2 → [a, a, $]
    for (var i = 0; i < terminals.length; i++) {
        tempArr.push("{0} -> [{1},{1},#]".f(axiom2,terminals[i]));
    }
    pr(tempArr);
    pr1();
    console.log("part 4/9 done");

// 5.1:  [q, ¢, X, a] → [¢, p, X, a], if (p, ¢, R) ∈ δ(q, ¢) and q ∈ Q \ F
    tempArr = [];
    for (var i = 0; i < states.length; i++) {
        for (var j = 0; j < all_states.length; j++) {
            for (var k = 0; k < tape_symbols.length; k++) {
                for (var l = 0; l < terminals.length; l++) {
                    if (containe_tr(states[i], '&', states[j], '&', 'r')) {
                        tempArr.push("[{0},&,{2},{3}] -> [&,{1},{2},{3}]"
                            .f(states[i], all_states[j], tape_symbols[k], terminals[l]));
                    }
                }
            }
        }
    }
    pr(tempArr);
// 5.2:  [¢, q, X, a] → [p, ¢, Y, a], if (p, Y, L) ∈ δ(q, X) and q ∈ Q \ F
    tempArr = [];
    for (var i = 0; i < states.length; i++) {
        for (var j = 0; j < all_states.length; j++) {
            for (var k1 = 0; k1 < tape_symbols.length; k1++) {
                for (var k2 = 0; k2 < tape_symbols.length; k2++) {
                    for (var l = 0; l < terminals.length; l++) {
                        if (containe_tr(states[i], tape_symbols[k1], states[j], tape_symbols[k2], 'l')) {
                            tempArr.push("[&,{0},{2},{4}] -> [{1},&,{3},{4}]"
                                .f(states[i], all_states[j], tape_symbols[k1], tape_symbols[k2], terminals[l]));
                        }
                    }
                }
            }
        }
    }
    pr(tempArr);
// 5.3:  [¢, q, X, a] [Z, b] → [¢, Y, a] [p, Z, b],  if (p, Y, R) ∈ δ(q, X) and q ∈ Q \ F
    tempArr = [];
    for (var i = 0; i < states.length; i++) {
        for (var j = 0; j < all_states.length; j++) {
            for (var k1 = 0; k1 < tape_symbols.length; k1++) {
                for (var k2 = 0; k2 < tape_symbols.length; k2++) {
                    for (var k3 = 0; k3 < tape_symbols.length; k3++) {
                        for (var l1 = 0; l1 < terminals.length; l1++) {
                            for (var l2 = 0; l2 < terminals.length; l2++) {
                                if (containe_tr(states[i], tape_symbols[k1], all_states[j], tape_symbols[k2], 'r')) {
                                    tempArr.push("[&,{0},{2},{5}][{4},{6}] -> [&,{3},{5}][{1},{4},{6}]"
                                        .f(states[i], all_states[j],
                                            tape_symbols[k1], tape_symbols[k2], tape_symbols[k3],
                                            terminals[l1], terminals[l2]));
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    pr(tempArr);
    pr1();
    console.log("part 5/9 done");

// 6.1:  [q, X, a] [Z, b] → [Y, a] [p, Z, b],        if (p, Y, R) ∈ δ(q, X) and q ∈ Q \ F
    tempArr = [];
    for (var i = 0; i < states.length; i++) {
        for (var j = 0; j < all_states.length; j++) {
            for (var k1 = 0; k1 < tape_symbols.length; k1++) {
                for (var k2 = 0; k2 < tape_symbols.length; k2++) {
                    for (var k3 = 0; k3 < tape_symbols.length; k3++) {
                        for (var l1 = 0; l1 < terminals.length; l1++) {
                            for (var l2 = 0; l2 < terminals.length; l2++) {
                                if (containe_tr(states[i], tape_symbols[k1], all_states[j], tape_symbols[k2], 'r')) {
                                    tempArr.push("[{0},{2},{5}][{4},{6}] -> [{3},{5}][{1},{4},{6}]"
                                        .f(states[i], all_states[j],
                                            tape_symbols[k1], tape_symbols[k2], tape_symbols[k3],
                                            terminals[l1], terminals[l2]));
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    pr(tempArr);
// 6.2:  [Z, b] [q, X, a] → [p, Z, b] [Y, a],        if (p, Y, L) ∈ δ(q, X) and q ∈ Q \ F
    tempArr = [];
    for (var i = 0; i < states.length; i++) {
        for (var j = 0; j < all_states.length; j++) {
            for (var k1 = 0; k1 < tape_symbols.length; k1++) {
                for (var k2 = 0; k2 < tape_symbols.length; k2++) {
                    for (var k3 = 0; k3 < tape_symbols.length; k3++) {
                        for (var l1 = 0; l1 < terminals.length; l1++) {
                            for (var l2 = 0; l2 < terminals.length; l2++) {
                                if (containe_tr(states[i], tape_symbols[k1], all_states[j], tape_symbols[k2], 'l')) {
                                    tempArr.push("[{4},{6}][{0},{2},{5}] -> [{1},{4},{6}][{3},{5}]"
                                        .f(states[i], all_states[j],
                                            tape_symbols[k1], tape_symbols[k2], tape_symbols[k3],
                                            terminals[l1], terminals[l2]));
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    pr(tempArr);
// 6.3:  [q, X, a] [Z, b, $] → [Y, a] [p, Z, b, $],  if (p, Y, R) ∈ δ(q, X) and q ∈ Q \ F
    tempArr = [];
    for (var i = 0; i < states.length; i++) {
        for (var j = 0; j < all_states.length; j++) {
            for (var k1 = 0; k1 < tape_symbols.length; k1++) {
                for (var k2 = 0; k2 < tape_symbols.length; k2++) {
                    for (var k3 = 0; k3 < tape_symbols.length; k3++) {
                        for (var l1 = 0; l1 < terminals.length; l1++) {
                            for (var l2 = 0; l2 < terminals.length; l2++) {
                                if (containe_tr(states[i], tape_symbols[k1], all_states[j], tape_symbols[k2], 'r')) {
                                    tempArr.push("[{0},{2},{5}][{4},{6},#] -> [{3},{5}][{1},{4},{6},#]"
                                        .f(states[i], all_states[j],
                                            tape_symbols[k1], tape_symbols[k2], tape_symbols[k3],
                                            terminals[l1], terminals[l2]));
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    pr(tempArr);
// 6.4:  [¢, Z, b] [q, X, a] → [¢, p, Z, b] [Y, a],  if (p, Y, L) ∈ δ(q, X) and q ∈ Q \ F
    tempArr = [];
    for (var i = 0; i < states.length; i++) {
        for (var j = 0; j < all_states.length; j++) {
            for (var k1 = 0; k1 < tape_symbols.length; k1++) {
                for (var k2 = 0; k2 < tape_symbols.length; k2++) {
                    for (var k3 = 0; k3 < tape_symbols.length; k3++) {
                        for (var l1 = 0; l1 < terminals.length; l1++) {
                            for (var l2 = 0; l2 < terminals.length; l2++) {
                                if (containe_tr(states[i], tape_symbols[k1], all_states[j], tape_symbols[k2], 'l')) {
                                    tempArr.push("[&,{4},{6}][{0},{2},{5}] -> [&,{1},{4},{6}][{3},{5}]"
                                        .f(states[i], all_states[j],
                                            tape_symbols[k1], tape_symbols[k2], tape_symbols[k3],
                                            terminals[l1], terminals[l2]));
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    pr(tempArr);
    pr1();
    console.log("part 6/9 done");

// 7.1:  [q, X, a, $] → [Y, a, p, $], if (p, Y, R) ∈ δ(q, X) and q ∈ Q \ F
    tempArr = [];
    for (var i = 0; i < states.length; i++) {
        for (var j = 0; j < all_states.length; j++) {
            for (var k1 = 0; k1 < tape_symbols.length; k1++) {
                for (var k2 = 0; k2 < tape_symbols.length; k2++) {
                    for (var l = 0; l < terminals.length; l++) {
                        if (containe_tr(states[i], tape_symbols[k1], all_states[j], tape_symbols[k2], 'r')) {
                            tempArr.push("[{0},{2},{4},#] -> [{3},{4},{1},#]"
                                .f(states[i], all_states[j], tape_symbols[k1], tape_symbols[k2], terminals[l]));
                        }
                    }
                }
            }
        }
    }
    pr(tempArr);
// 7.2:  [X, a, q, $] → [p, X, a, $], if (p, $, L) ∈ δ(q, $) and q ∈ Q \ F
    tempArr = [];
    for (var i = 0; i < states.length; i++) {
        for (var j = 0; j < all_states.length; j++) {
            for (var k = 0; k < tape_symbols.length; k++) {
                for (var l = 0; l < terminals.length; l++) {
                    if (containe_tr(states[i], '#', all_states[j], '#', 'l')) {
                        tempArr.push("[{2},{3},{0},#] -> [{1},{2},{3},#]"
                            .f(states[i], all_states[j], tape_symbols[k], terminals[l]));
                    }
                }
            }
        }
    }
    pr(tempArr);
// 7.3:  [Z, b] [q, X, a, $] → [p, Z, b] [Y, a, $],  if (p, Y, L) ∈ δ(q, X) and q ∈ Q \ F
    tempArr = [];
    for (var i = 0; i < states.length; i++) {
        for (var j = 0; j < all_states.length; j++) {
            for (var k1 = 0; k1 < tape_symbols.length; k1++) {
                for (var k2 = 0; k2 < tape_symbols.length; k2++) {
                    for (var k3 = 0; k3 < tape_symbols.length; k3++) {
                        for (var l1 = 0; l1 < terminals.length; l1++) {
                            for (var l2 = 0; l2 < terminals.length; l2++) {
                                if (containe_tr(states[i], tape_symbols[k1], all_states[j], tape_symbols[k2], 'l')) {
                                    tempArr.push("[{4},{6}][{0},{2},{5},#] -> [{1},{4},{6}][{3},{5},#]"
                                        .f(states[i], all_states[j],
                                            tape_symbols[k1], tape_symbols[k2], tape_symbols[k3],
                                            terminals[l1], terminals[l2]));
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    pr(tempArr);
    pr1();
    console.log("part 7/9 done");

// 8.1:  [q, ¢, X, a] → a,   if q ∈ F
    tempArr = [];
    for (var i = 0; i < finite_states.length; i++) {
        for (var j = 0; j < tape_symbols.length; j++) {
            for (var k = 0; k < terminals.length; k++) {
                tempArr.push("[{0},&,{1},{2}] -> {2}".f(finite_states[i], tape_symbols[j], terminals[k]));
            }
        }
    }
    pr(tempArr);
// 8.2:  [¢, q, X, a] → a,   if q ∈ F
    tempArr = [];
    for (var i = 0; i < finite_states.length; i++) {
        for (var j = 0; j < tape_symbols.length; j++) {
            for (var k = 0; k < terminals.length; k++) {
                tempArr.push("[&,{0},{1},{2}] -> {2}".f(finite_states[i], tape_symbols[j], terminals[k]));
            }
        }
    }
    pr(tempArr);
// 8.3:  [q, X, a] → a, if q ∈ F
    tempArr = [];
    for (var i = 0; i < finite_states.length; i++) {
        for (var j = 0; j < tape_symbols.length; j++) {
            for (var k = 0; k < terminals.length; k++) {
                tempArr.push("[{0},{1},{2}] -> {2}".f(finite_states[i], tape_symbols[j], terminals[k]));
            }
        }
    }
    pr(tempArr);
// 8.4:  [q, X, a, $] → a,   if q ∈ F
    tempArr = [];
    for (var i = 0; i < finite_states.length; i++) {
        for (var j = 0; j < tape_symbols.length; j++) {
            for (var k = 0; k < terminals.length; k++) {
                tempArr.push("[{0},{1},{2},#] -> {2}".f(finite_states[i], tape_symbols[j], terminals[k]));
            }
        }
    }
    pr(tempArr);
// 8.5:  [X, a, q, $] → a,   if q ∈ F
    tempArr = [];
    for (var i = 0; i < finite_states.length; i++) {
        for (var j = 0; j < tape_symbols.length; j++) {
            for (var k = 0; k < terminals.length; k++) {
                tempArr.push("[{1},{2},{0},#] -> {2}".f(finite_states[i], tape_symbols[j], terminals[k]));
            }
        }
    }
    pr(tempArr);
    pr1();
    console.log("part 8/9 done");

// 9.1:  a[X, b] → ab
    tempArr = [];
    for (var i = 0; i < tape_symbols.length; i++) {
        for (var k1 = 0; k1 < terminals.length; k1++) {
            for (var k2 = 0; k2 < terminals.length; k2++) {
                tempArr.push("{1}[{0},{2}] -> {1}{2}".f(tape_symbols[i], terminals[k1], terminals[k2]));
            }
        }
    }
    pr(tempArr);
// 9.2:  a[X, b, $] → ab
    tempArr = [];
    for (var i = 0; i < tape_symbols.length; i++) {
        for (var k1 = 0; k1 < terminals.length; k1++) {
            for (var k2 = 0; k2 < terminals.length; k2++) {
                tempArr.push("{1}[{0},{2},#] -> {1}{2}".f(tape_symbols[i], terminals[k1], terminals[k2]));
            }
        }
    }
    pr(tempArr);
// 9.3:  [X, a]b → ab
    tempArr = [];
    for (var i = 0; i < tape_symbols.length; i++) {
        for (var k1 = 0; k1 < terminals.length; k1++) {
            for (var k2 = 0; k2 < terminals.length; k2++) {
                tempArr.push("[{0},{1}]{2} -> {1}{2}".f(tape_symbols[i], terminals[k1], terminals[k2]));
            }
        }
    }
    pr(tempArr);
// 9.4:  [¢, X, a]b → ab
    tempArr = [];
    for (var i = 0; i < tape_symbols.length; i++) {
        for (var k1 = 0; k1 < terminals.length; k1++) {
            for (var k2 = 0; k2 < terminals.length; k2++) {
                tempArr.push("[&,{0},{1}]{2} -> {1}{2}".f(tape_symbols[i], terminals[k1], terminals[k2]));
            }
        }
    }
    pr(tempArr);

    console.log("part 9/9 done");
}

