var fs = require('fs');
var lineReader = require('line-reader');

function produceAndWriteToFile() {
    var counter = 0;
    lineReader.eachLine('mt.txt', function(line, last) {
        if (counter++ === 0) {
            line = line.substr(1);
            func(line);
        } else {
            if (line != "\n") func(line);
        }

        if (last) {
            generate();
            return false; // stop reading
        }
    });
}

function func(data) {
    data = parseData(data);
    // console.log(data);
    if (data != undefined) {
        var g_rules = generate_grammar_rules(data);
        for (var i = 0; i < g_rules.length; i++) {
            G.push(g_rules[i]);
        }
    }
}

function parseData(data) {
    if (data === "") return;
    var arrResult = [], arrTemp = [];
    arrTemp = data.split("->");
    arrResult.push(arrTemp[0][0]);
    arrResult.push(arrTemp[0].replace(/^(0|1|B|#|b|&|a)/, ""));
    arrResult.push(arrTemp[1][0]);
    arrResult[4] = arrTemp[1][arrTemp[1].length-1];
    arrTemp[1] = arrTemp[1].replace(/^(0|1|B|#|b|&|a)/, "");
    arrTemp[1] = arrTemp[1].replace(/(L|R)$/, "");
    arrResult[3] = arrTemp[1];

    return arrResult;
}

var tape_alphabet = ['0', '1', 'B', 'a', 'b', '&', '#'];
var input_alphabet = ['0', '1', 'B'];

var symbols = [];
for (var i = 0; i < input_alphabet.length; i++) {
    for (var j = 0; j < tape_alphabet.length; j++) {
        symbols.push("[" + input_alphabet[i] + "," + tape_alphabet[j] + "]");
    }
}

function init_rules() {
    var gramArray = [];
    gramArray.push("S -> [B,B]S\n");
    gramArray.push("S -> S[B,B]\n");
    gramArray.push("S -> q1\n");

    return gramArray;
}

function generate_grammar_rules(rule) {
    var cstate = rule[1];
    var cletter = rule[0];
    var nletter = rule[2];
    var dir = rule[4];
    var nstate = rule[3];
    var returnRules = [];

    switch (dir) {
        case 'L' : {
            for (var i = 0; i < symbols.length; i++) {
                for (var j = 0; j < input_alphabet.length; j++) {
                    returnRules.push("{0}{1}[{2},{3}] -> {4}{5}[{6},{7}]\n".f(symbols[i], cstate, input_alphabet[j], cletter, nstate, symbols[i], input_alphabet[j], nletter));
                }
            }

            break;
        }

        case 'R' : {
            for (var i = 0; i < input_alphabet.length; i++ ) {
                returnRules.push("{0}[{1},{2}] -> [{3},{4}]{5}\n".f(cstate, input_alphabet[i], cletter, input_alphabet[i], nletter, nstate));
            }
            break;
        }
    }

    return returnRules;
}


String.prototype.format = String.prototype.f = function(){
    var args = arguments
    return this.replace(/\{(\d+)\}/g, function(m,n){
        return args[n] ? args[n] : m;
    });
};